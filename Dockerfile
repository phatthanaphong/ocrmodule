FROM tiangolo/uwsgi-nginx-flask:python3.6
MAINTAINER Joe <phatthanaphong.c@gmail.com>

ENV STATIC_INDEX 1
# Install tensorflow
RUN pip install --upgrade \
  https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-1.4.0-cp36-cp36m-linux_x86_64.whl

# Install dependencies
RUN pip install --upgrade numpy
RUN pip install --upgrade pandas
RUN pip install opencv-python
RUN pip install --upgrade tflearn
RUN pip install --upgrade h5py

#Copy Source
COPY ./src /src
COPY ./app /app