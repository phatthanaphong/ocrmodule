from flask import Flask, send_file, request, render_template
from werkzeug import secure_filename
import os
import sys
import uuid

sys.path.append("../src/")
from LoadModel import loadModel
from CharRecognition import CharRecognition

#model = 0

app = Flask(__name__)

@app.route("/hello")
def hello() :
    return "Hello World from Flask"

@app.route("/doocr", methods = ['GET','POST'])
def  doOCR():
    global model 
    fn = request.args.get("filename")
    fullpath = os.path.join("../src/images/",fn)
    out   = CharRecognition(fullpath, model)
    res = ''
    for l in out:
        for lp in l:
            res = res+lp
        res = res+'\n'
    #s = unicode(tmp, "utf-8")
    return res

## ORR by hrml form :: upload image to ../src/images
@app.route("/doocrform", methods = ['GET','POST'])
def  doOCRForm(fn):
    fullpath = os.path.join("../src/images/",fn)
    out   = CharRecognition(fullpath, model)
    res = ''
    for l in out:
        for lp in l:
            res = res+lp
        res = res+'\n'
    return res

## Form to upload a single file
@app.route('/uploadimage')
def upload_form():
   index_path = os.path.join(app.static_folder, 'upload.html')
   return send_file(index_path)

@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
   if request.method == 'POST':
      f = request.files['file']
      f.filename =  os.path.join('',uuid.uuid4().hex+'.jpg')
      f.save(os.path.join('../src/images/',secure_filename(f.filename)))
      res = doOCRForm(f.filename)
      #delete file from the temp folder after performing the ocr
      os.system('rm '+os.path.join('../src/images/',secure_filename(f.filename)))
      return res 

@app.route("/")
def main():
    index_path = os.path.join(app.static_folder, 'index.html')
    return send_file(index_path)

if __name__ == "__main__":
    #Only for debugging while developing
    #model = loadModel('../src/charmodelmix55.cnn')
    app.run(host='0.0.0.0', debug=True, port=80)
    #app.run()
