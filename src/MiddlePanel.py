import wx
from RightPanel import RightPanel
from LeftPanel import LeftPanel
##from contourCharAndRecognize import process
from CharRecognition import CharRecognition

class MiddlePanel(wx.Panel):

    rpanel = None
    lpanel = None
    model = None
    
    def __init__(self, parent, r, l):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = (0,0),
                            size = wx.Size( 40,600 ), style = wx.TAB_TRAVERSAL )
        self.rpanel = r
        self.lpanel = l
        self.SetBackgroundColour(wx.WHITE)
        self.initGUI()

    def initGUI(self):

        btnSize = ((150, -1));
        self.btn1 = wx.Button(self, -1, "Recognition",  size=btnSize)
##        self.btn2 = wx.Button(self, -1, "Character Segmentation")
##        self.btn3 = wx.Button(self, -1, "Support Vector Machine")
##        self.btn4 = wx.Button(self, -1, "Ensamble : Random Forest")
##        self.btn5 = wx.Button(self, -1, "Neural Network")
##        self.btn6 = wx.Button(self, -1, "Convolution Neural Network")
        sizer = wx.BoxSizer(wx.VERTICAL)


        self.Bind(wx.EVT_BUTTON, self.Recognition, self.btn1)
        

        sizer.AddSpacer(20)
        sizer.Add(self.btn1, 0, wx.ALIGN_CENTER)
##
##        sizer.AddSpacer(20)
##        sizer.Add(self.btn2, 0, wx.ALIGN_CENTER)
##
##        sizer.AddSpacer(20)
##        sizer.Add(self.btn3, 0, wx.ALIGN_CENTER)
##
##        sizer.AddSpacer(20)
##        sizer.Add(self.btn4, 0, wx.ALIGN_CENTER)
##
##        sizer.AddSpacer(20)
##        sizer.Add(self.btn5, 0, wx.ALIGN_CENTER)
##
##        sizer.AddSpacer(20)
##        sizer.Add(self.btn6, 0, wx.ALIGN_CENTER)
        
        self.SetSizer(sizer)

    def SetModel(self, md):
        self.model = md

    def Recognition(self, e):
        fn = self.lpanel.GetFileName()
        #out = process(fn, self.model)
        out = CharRecognition(fn, self.model)
##        self.rpanel.SetText(out)
##        self.rpanel.Update()
##        self.rpanel.Refresh() 
        self.rpanel.SetData(out)

     
