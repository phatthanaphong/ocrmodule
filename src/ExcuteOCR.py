from CharRecognition import CharRecognition
import sys, json
from ctypes import *
from LoadModel import loadModel
import warnings
warnings.filterwarnings("ignore")
import os
os.environ["TF_CPP_MIN_LOG_LEVEL"]="2"

def excuteOCR(fn):
    model = loadModel('charmodelmix55.cnn') 
    out   = CharRecognition(fn, model)
    tmp = ''
    for l in out:
            for lp in l:
               tmp = tmp+lp
            tmp = tmp+'\n'
    return tmp


def excuteOCRWithList(fn):
    model = loadModel('charmodelmix55.cnn')
    print(fn)
    #out   = CharRecognition(fn, model)
    #return out
