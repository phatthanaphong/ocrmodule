from CharRecognition import CharRecognition
import sys, json
from ctypes import *
from LoadModel import loadModel
import warnings
warnings.filterwarnings("ignore")
import os
os.environ["TF_CPP_MIN_LOG_LEVEL"]="2"

if len(sys.argv) == 2:
    fn  = sys.argv[1]
    #fn  = 'im1.jpg'
    model = loadModel('../src/charmodelmix55.cnn') 
    out   = CharRecognition(fn, model)
    tmp = ''
    for l in out:
        for lp in l:
            tmp = tmp+lp
        tmp = tmp+'\n'
    #s = unicode(tmp, "utf-8")
    print(tmp)

    ##def excuteOCR(fn):
    ##    model = loadModel('charmodelmix55.cnn') 
    ##    out   = CharRecognition(fn, model)
    ##    tmp = ''
    ##    for l in out:
    ##        for lp in l:
    ##            tmp = tmp+lp
    ##        tmp = tmp+'\n'
    ##    return tmp
else:
    print('Pleae input an image file')
