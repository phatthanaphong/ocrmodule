import wx
from LeftPanel import LeftPanel
from RightPanel import RightPanel
from MiddlePanel import MiddlePanel
import os, os.path
import numpy as np
import pandas as pd
#from scipy.misc import imread, imshow
from sklearn.metrics import accuracy_score
import tensorflow as tf
from PIL import Image
import matplotlib.pyplot as plt
from LoadModel import loadModel


class Example(wx.Frame):

   imagefile = None
   lpanel    = None
   rpanel    = None
   mpanel    = None
   image     = None
   model1 = None
   model2 = None
   flag = False
            
   def __init__(self,parent): 
      super(Example, self).__init__(parent)  
      self.InitUI()
      self.Maximize(True)
           
   def InitUI(self):
      print("loading a model, please wait")
      #self.model1 = loadModel('charmodelmix3layers.cnn')
      self.model1 = loadModel('charmodelmix55.cnn')
      #self.model2 = loadModel('charmodelmix3layers_ei_ee.cnn')
      #self.Bind(wx.EVT_MOVE, self.OnMove) 
      #self.SetSize((800, 700)) 
      self.SetTitle('Read Thai Texts') 
      self.Centre()
      print("the model is loaded")

      #file connect to server
      con   = wx.Menu()
      connection = con.Append(wx.ID_OPEN, "&Conection", "Open Connection")
      #file menu

       #file menu
      ocrtrain   = wx.Menu()
      openfolder = ocrtrain.Append(wx.ID_OPEN, "&Open Folder", "Open image files")
      #file menu
      
      filemenu = wx.Menu()
      openfile = filemenu.Append(wx.ID_FILE, "&Open File", "Open image files")
      filemenu.Append(wx.ID_ABOUT, "&About", "About the program")
      #filemenu.AppendSeparator()
      #itemExit = filemenu.Append(wx.ID_EXIT, "&Exit", "Close the program")

       #file menu
      ocrtrain   = wx.Menu()
      openfolder = ocrtrain.Append(wx.ID_FILE1, "&Train", "Train the model")

      sen  = wx.Menu()
      sentenencetrain = sen.Append(wx.ID_FILE2, "&Train", "Train the model")

      ex  = wx.Menu()
      exitp = ex.Append(wx.ID_EXIT, "&Exit", "Exit the program")

      menubar = wx.MenuBar()
      menubar.Append(con, "&Connection")
      menubar.Append(filemenu, "&Printed OCR")
      menubar.Append(ocrtrain, "&Train OCR")
      menubar.Append(sen, "&Train Sentence")
      menubar.Append(ex, "&Exit")

      #Enable(filemenu,False)
      self.SetMenuBar(menubar)

      self.Bind(wx.EVT_MENU, self.onConnection, connection)
      #self.Bind(wx.EVT_MENU, self.OnExit, itemExit)
      self.Bind(wx.EVT_MENU, self.OnOpenFile, openfile)
      self.Bind(wx.EVT_MENU, self.OnExit, exitp)
      self.Bind(wx.EVT_MENU, self.onTrainOCR, openfolder)
      
      
      self.Show(True)

   def OnExit(self, e):
      self.Close(True)
    

   def OnMove(self, e): 
      x, y = e.GetPosition() 
      print("current window position x = %d y = %d "%(x,y))

   def onTrainOCR(self, e):
      dlg = LoginDialog()
      dlg.ShowModal()
      

   def onConnection(self, e):

      dlg = wx.TextEntryDialog(self, 'Enter host','Connecting to a server')
      #dlg.SetValue("Default")
      if dlg.ShowModal() == wx.ID_OK:
         #print('You entered: %s\n' % dlg.GetValue())
         if dlf.GetValue == "202.28.34.202":
            flag = True
            wx.MessageBox("Connection is success")
         else : print("Connection Failed")
            
      
      dlg.Destroy()
      

   def OnOpenFile(self, e):
       openFileDialog = wx.FileDialog(self, "Open", "", "", 
                                       "All files (*.jpg)|*.jpg", 
                                       wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
       openFileDialog.ShowModal()
       imagefile = openFileDialog.GetPath()
       openFileDialog.Destroy()

       # setlayout
       self.lpanel = LeftPanel(self)
       self.rpanel = RightPanel(self)
       self.mpanel = MiddlePanel(self, self.rpanel, self.lpanel)
      
       sz = wx.BoxSizer(wx.HORIZONTAL)
       sz.Add(self.lpanel,0,wx.EXPAND)
       sz.Add(self.mpanel,1,wx.EXPAND)
       sz.Add(self.rpanel,2,wx.EXPAND)

       self.SetSizer(sz)
       self.Layout()
       self.Fit()
       
       self.rpanel.ClearText()
       self.lpanel.SetImage(imagefile)

       
       self.lpanel.Update()
       self.lpanel.Refresh()
       self.mpanel.SetModel(self.model1)

       
      
       '''
       if(self.image != None):
          self.image.Destroy()
       self.image = wx.StaticBitmap(self.lpanel, -1, wx.Bitmap(imagefile, wx.BITMAP_TYPE_ANY))
       self.lpanel.Update()
       sizer = wx.BoxSizer(wx.VERTICAL)
       sizer.Add(self.image, 0, wx.ALIGN_CENTER)
       self.lpanel.SetSizer(sizer)
       '''

class LoginDialog(wx.Dialog):
    """
    Class to define login dialog
    """
 
    #----------------------------------------------------------------------
    def __init__(self):
        """Constructor"""
        wx.Dialog.__init__(self, None, title="Training Configuration")
 
        # user info
        data_sizer = wx.BoxSizer(wx.HORIZONTAL)
 
        data_lbl = wx.StaticText(self, label="Data Directory:")
        data_sizer.Add(data_lbl, 0, wx.ALL|wx.CENTER, 5)
        self.data = wx.TextCtrl(self, 10)
        data_sizer.Add(self.data, 0, wx.ALL, 5)
 
        # pass info
        e_sizer = wx.BoxSizer(wx.HORIZONTAL)
 
        e_lbl = wx.StaticText(self, label="Epoch:              ")
        e_sizer.Add(e_lbl, 0, wx.ALL|wx.CENTER, 5)
        self.e = wx.TextCtrl(self)
        e_sizer.Add(self.e, 0, wx.ALL, 5)


       # pass info
        p_sizer = wx.BoxSizer(wx.HORIZONTAL)
 
        p_lbl = wx.StaticText(self, label="Training Type: ")
        p_sizer.Add(p_lbl, 0, wx.ALL|wx.CENTER, 5)
        #self.pp = wx.TextCtrl(self)
        languages = ['CPU - ADAM', 'GPU - ADAM', 'CPU - CGD', 'GPU - CDG'] 
        self.combo = wx.ComboBox(self,choices = languages) 	
        #box.Add(self.combo,1,wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL|wx.ALL,5) 
        p_sizer.Add(self.combo, 0, wx.ALL, 5)

          # pass info
        m_sizer = wx.BoxSizer(wx.HORIZONTAL)
 
        m_lbl = wx.StaticText(self, label = "Model:              ")
        m_sizer.Add(m_lbl, 0, wx.ALL|wx.CENTER, 5)
        self.mm = wx.TextCtrl(self)
        m_sizer.Add(self.mm, 0, wx.ALL, 5)
 
        main_sizer = wx.BoxSizer(wx.VERTICAL)
        main_sizer.Add(data_sizer, 0, wx.ALL, 5)
        main_sizer.Add(e_sizer, 0, wx.ALL, 5)
        main_sizer.Add(p_sizer, 0, wx.ALL, 5)
        main_sizer.Add(m_sizer, 0, wx.ALL, 5)
 
        btn = wx.Button(self, label="Train")
        #btn.Bind(wx.EVT_BUTTON, self.onLogin)
        main_sizer.Add(btn, 0, wx.ALL|wx.CENTER, 5)
 
        self.SetSizer(main_sizer)
 

ex = wx.App()
Example(None) 
ex.MainLoop()
