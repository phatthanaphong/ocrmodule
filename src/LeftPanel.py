import wx

class LeftPanel(wx.Panel):

    img = None
    imgx = 1
    imgy = 1
    filename = None
    model = None
    
    def __init__(self, parent):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = (0,0), size = wx.Size( 560,600 ), style = wx.TAB_TRAVERSAL )
        #self.SetBackgroundColour(wx.RED)
        self.Displayimage()

    def Displayimage(self):
        #print("load image done");
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        #self.png = wx.StaticBitmap(self, -1, wx.Bitmap("D:/python_tutorial/imageprocess/image.jpg", wx.BITMAP_TYPE_ANY))

    def SetImage(self,fn):
        self.filename = fn
        self.img = wx.Image(self.filename, wx.BITMAP_TYPE_ANY)
        self.imgx, self.imgy = self.img.GetSize()

    def GetFileName(self):
        return self.filename
 
    def OnPaint(self, event):
        #print("working here")
        if(self.img != None):
            dc = wx.PaintDC(self)
            dc.Clear()
            x,y = self.GetSize()
            posx,posy = 0, 0
            newy = int(float(x)/self.imgx*self.imgy)
            if newy < y:
                posy = int((y - newy) / 2) 
                y = newy
            else:
                newx = int(float(y)/self.imgy*self.imgx)
                posx = int((x - newx) / 2)
                x = newx        

            img = self.img.Scale(x,y, wx.IMAGE_QUALITY_HIGH)
            self.bmp = wx.Bitmap(img)
            dc.DrawBitmap(self.bmp,posx,posy)

        
