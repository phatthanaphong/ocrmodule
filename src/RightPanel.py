import wx

class RightPanel(wx.Panel):

    img = None
    imgx = 1
    imgy = 1
    text = []
    editname = None
    
    def __init__(self, parent):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = (10,0), size = wx.Size( 100,100 ), style = wx.TAB_TRAVERSAL )
        #self.SetBackgroundColour(wx.BLUE)
        self.InitUI()
        self.Bind(wx.EVT_PAINT, self.OnPaint)

    def InitUI(self):
        self.editname = wx.TextCtrl(self, size=(550, 700), style= wx.TE_MULTILINE | wx.SUNKEN_BORDER)
        self.windowSizer = wx.BoxSizer()
        self.windowSizer.Add(self, 1, wx.ALL | wx.EXPAND)        

        # Set sizer for the panel content
        self.sizer = wx.GridBagSizer(1, 1)
        self.sizer.Add(self.editname, (0, 0))

        # Set simple sizer for a nice border
        self.border = wx.BoxSizer()
        self.border.Add(self.sizer, 1, wx.ALL | wx.EXPAND, 5)

        # Use the sizers
        self.SetSizerAndFit(self.border)

    def SetData(self,txt):
        print(len(txt))
        font1 = wx.Font(16, wx.MODERN, wx.NORMAL, wx.NORMAL, False, u'Consolas')
        self.editname.SetFont(font1)
        self.text = txt
        self.editname.SetValue('')
        try:
            for a in self.text :
                    for b in a :
                        self.editname.AppendText(b)
                    self.editname.AppendText('\n')
        except Exception as e:
            print(e)
            
    def ClearText(self):
        self.editname.SetValue("")

    def SetImage(self,fileName):
        self.img = wx.Image(fileName, wx.BITMAP_TYPE_ANY)
        self.imgx, self.imgy = self.img.GetSize()

    def SetText(self, txt):
        self.text = txt

    def OnPaint(self, event):
        #print("working here")
        #if(self.img != None):
            dc = wx.PaintDC(self)
            dc.Clear()
            x = 0
            y = 0
            dc.DrawText("This is the output ", 20, 10)
            x = x + 25
            y = y + 20
            for a in self.text :
                for b in a :
                    dc.DrawText(b,x,y)
                    x=x+5
                x = 25
                y = y + 20
 
##    def OnPaint(self, event):
##        #print("working here")
##        if(self.img != None):
##            dc = wx.PaintDC(self)
##            dc.Clear()
##            x,y = self.GetSize()
##            posx,posy = 0, 0
##            newy = int(float(x)/self.imgx*self.imgy)
##            if newy < y:
##                posy = int((y - newy) / 2) 
##                y = newy
##            else:
##                newx = int(float(y)/self.imgy*self.imgx)
##                posx = int((x - newx) / 2)
##                x = newx        
##
##            img = self.img.Scale(x,y, wx.IMAGE_QUALITY_HIGH)
##            self.bmp = wx.Bitmap(img)
##            dc.DrawBitmap(self.bmp,posx,posy)

