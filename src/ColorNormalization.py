import numpy as np
import cv2
import os
import sys
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
from PIL import Image
import scipy
from scipy.optimize import leastsq
from scipy.stats.mstats import gmean
from scipy.signal import argrelextrema
from scipy.stats import entropy
from scipy.signal import savgol_filter


root = './images'
image = 'im3.jpg'
template = 'template.jpg'

########################
# Normalization function
########################

    

def normalization(img):

    #-----------------------------------
    ## 1. Create Chromaticity Vectors ##
    #-----------------------------------

    # Get Image
    #img = cv2.imread(os.path.join(root, image))
    #img_yuv = cv2.cvtColor(img, cv2.COLOR_RGB2YUV)

    img1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    h = cv2.equalizeHist(img1)
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    #img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    h1, w1 = img.shape[:2]
    

    temp = cv2.imread(os.path.join(root, template))
    temp = cv2.cvtColor(temp, cv2.COLOR_BGR2RGB)
    h2, w2 = temp.shape[:2]

##    r1, g1, b1 = cv2.split(temp)
##    red = cv2.equalizeHist(r1)
##    grn = cv2.equalizeHist(g1)
##    blu = cv2.equalizeHist(b1)
##    temp = cv2.merge((red,grn,blu))

    out = img.copy()
    

    # Separate Channels
    r1, g1, b1 = cv2.split(img)  # image
    r2, g2, b2 = cv2.split(temp) # template

    #print(r1.shape)

    m_r1 = np.mean(r1.flatten())
    m_g1 = np.mean(g1.flatten())
    m_b1 = np.mean(b1.flatten())

    #print(m_r1, m_g1, m_b1)

    m_r2 = np.mean(r2.flatten())
    m_g2 = np.mean(g2.flatten())
    m_b2 = np.mean(b2.flatten())

    #print(m_r2, m_g2, m_b2)
    
    s_r1 = np.std(r1.flatten())
    s_g1 = np.std(g1.flatten())
    s_b1 = np.std(b1.flatten())

    s_r2 = np.std(r2.flatten())
    s_g2 = np.std(g2.flatten())
    s_b2 = np.std(b2.flatten())
    
    for i in range(h1):
        for j in range(w1):
            
            r = img[i,j,0]
            g = img[i,j,1]
            b = img[i,j,2]
 
            tr = r - m_r1
            tg = g - m_g1
            tb = b - m_b1
            

            lr = (s_r2/s_r1)*tr
            lg = (s_g2/s_g1)*tg
            lb = (s_b2/s_b1)*tb

            llr = lr + m_r2
            llg = lg + m_g2
            llb = lb + m_b2

            if  llr > 250:
                llr = 250
            if  llr < 0:
                llr = 0

            if  llg > 250:
                llg = 250
            if  llg < 0:
                llg = 0

            if  llb > 250:
                llb = 250
            if  llb < 0:
                llb = 0

            out[i,j,2] = llr
            out[i,j,1] = llg
            out[i,j,0] = llb

    out1 = cv2.cvtColor(out, cv2.COLOR_RGB2BGR)
    return temp

if __name__ == '__main__':
    img = cv2.imread(os.path.join(root, image))
    out = normalization(img)
    out1 = cv2.cvtColor(out, cv2.COLOR_RGB2BGR)

    plt.imshow(img)
    plt.title('original')
    plt.show()

    plt.imshow(out1)
    plt.title('output')
    plt.show()
